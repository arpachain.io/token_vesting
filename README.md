# VestingToken SmartContract

This is a smart contract for vesting token use.  
There is a owner, beneficiary. Owner create an instance of this smart contract. Beneficiary receives tokens periodly.  

## Contract parameters
Parameter | Description
----------|------------
`beneficiary` | Person who will get token periodly
`start` | Token start release to beneficary Unix timestamp
`end` | Token finish release to beneficary Unix timestamp
`stage` | Number of period release process will last
`revocable` | `true` or `false` in case beneficiary lost his/her wallet pri-key

## How to use

- Owner create smart contract at Eth Mainnet
- Owner transfer tokens to **smart contract address**
- Owner or beneficiary call smart contract function `release` to release token step by step